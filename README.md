# gotoandplay-test-task

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn run serve
```

### Compiles and minifies for production
```
yarn run build
```

### Lints and fixes files
```
yarn run lint
```

## Kasutusjuhend
### Matemaatika
Vastuse saamiseks peaks sisestama ainult tehte, sõnu kasutamata
```
5+5
5 - 9
9*9
5/4
```
### Kellaeg
Kellaja saamiseks tuleb seda küsida (kuulatakse sõna "time")
````
Whats the time?

````
### Ilmateade
Ilmateate saamiseks tuleb seda küsida. 
Kuulatakse sõnu 'weather', 'sunny', 'cloudy', 'rainy', 'snowy'.
Küsimuse lõpus peab olema Linn.
````
Weather Tallinn
Is it sunny in Tokyo?
cloudy dublin
````
### Small talk
Kuulatakse järgmisi sõnesid:
````
Who are you?
How old are you?
You're annoying.
You're beautiful.
What's your birth date?
Bye-bye!
Good evening!
Good morning!
Good night!
Hello!
How are you?
Nice to meet you!
Nice to see you!
Nice to talk to you!
What's up?
````


